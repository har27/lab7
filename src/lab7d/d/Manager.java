/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package lab7d.d;

/**
 *
 * @author harpreet singh
 */
public class Manager extends  Employee {
    private double bonus;
    
    public Manager (String name ,double hourlyWage , double hoursWorked, double bonus)
    {
    super(name, hourlyWage, hoursWorked);
    this.bonus=bonus;
    }

    public double getBonus(){
        return bonus;
    }
    
    @Override
    public double calculatePay(){
        return super.calculatePay()+getBonus();
    }
    
}
